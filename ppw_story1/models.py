from django.db import models

# Create your models here.
class JadwalModel(models.Model):
    aktifitas = models.CharField(max_length = 30)
    date = models.CharField(max_length = 30)

    def __str__(self):
        return self.aktifitas + ' - ' + self.date
