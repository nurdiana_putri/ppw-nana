from django.shortcuts import render,redirect
from datetime import datetime, date
from .models import JadwalModel

# Create your views here.
def index(request):
    response = {}
    return render(request, 'landing.html', response)


def indexStory1(request):
    response = {}
    return render(request, 'story1.html', response)

def indexStory3(request):
    response = {}
    return render(request, 'story3.html', response)

def indexJadwal(request):
    aktifitas = JadwalModel.objects.order_by('-id')
    return render(request, 'form.html', {'aktifitas': aktifitas})


def addJadwal(request):
    if request.method == 'POST':
        aktifitas = request.POST['aktifitas']
        date = request.POST['date']
        data = {
            'aktifitas': aktifitas,
            'date' : date
        }
        JadwalModel.objects.create(aktifitas=data['aktifitas'], date=data['date'])
        return indexJadwal(request)
    return redirect('/')
