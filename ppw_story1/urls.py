from django.conf.urls import url
from django.urls import path
from .views import *


urlpatterns = [
	path('', index, name='index'),
	path('indexStory1/', indexStory1, name='indexStory1'),
	path('indexStory3/', indexStory3, name='indexStory3'),
	path('indexJadwal/', indexJadwal, name='indexJadwal'),
	path('addJadwal/', addJadwal, name='addJadwal'),
]
